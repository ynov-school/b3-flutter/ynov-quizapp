import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


   // Need La methode pour envoyer des mails de Google




class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

   

    return Scaffold(
            body: Container(
        padding: EdgeInsets.all(30),
        decoration: BoxDecoration(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "Une suggestion ? Un Problème ? Un avis ? N'hésitez plus et contactez nous :",
              style: Theme.of(context).textTheme.headline5,
              textAlign: TextAlign.center,
            ),
            MailButton(
              text: 'Nous Contacter',
              icon: FontAwesomeIcons.envelope,
              color: Colors.black45,
       //       ContactMethod: ,
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('À propos'),
        backgroundColor: Colors.cyan[700],
      ),
    );
  }
}



class MailButton extends StatelessWidget {
  final Color color;
  final IconData icon;
  final String text;
  //final Function ContactMethod;

  const MailButton({this.text, this.icon, this.color/*, this.ContactMethod*/});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: FlatButton.icon(
        padding: EdgeInsets.all(30),
        icon: Icon(icon, color: Colors.white),
        color: color,
        onPressed: (){}, // email sender
        label: Expanded(
          child: Text(
            '$text',
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}